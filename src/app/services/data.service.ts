import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Channel } from '../models/data';

@Injectable()
export class DataService{
    private readonly BASE_URL = 'https://api.persik.by/v2/';

    constructor(private http: HttpClient){}

    getChannels(): Promise<Channel[]>{
        return this.http.get<any>(this.BASE_URL.concat('content/channels')).pipe(map(res => res.channels)).toPromise();
    }
}

interface ServerChannels{
    channels :Channel[];
}